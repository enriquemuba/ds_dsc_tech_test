
import sqlite3
import pandas as pd
from contextlib import contextmanager

DB_FILEPATH = './database.sqlite'
MOD_COMMANDS = ["CREATE", "ALTER", "DROP", "INSERT", "UPDATE", "DELETE"]


@contextmanager
def get_db_connection(db_filepath: str = DB_FILEPATH) -> sqlite3.Connection:
    """Yields an sqlite3 connection to the database stored in db_filepath.

    Parameters
    ----------
    db_filepath : str, optional
        file path where the splite database is stored, by default DB_FILEPATH

    Yields
    ------
    sqlite3.Connection
    """
    try:
        with sqlite3.connect(db_filepath) as conn:
            yield conn
    finally:
        conn.close()


def fetch_query(sql: str, db_filepath: str = DB_FILEPATH,
                allow_modifications: bool = False) -> pd.DataFrame:
    """Returns a pandas form of a FETCH type SQLite3 query.

    Parameters
    ----------
    sql : str
        SQLite query to be executed
    db_filepath : str, optional
        file path where the splite database is stored, by default DB_FILEPATH

    Returns
    -------
    pd.DataFrame
    """
    if not allow_modifications and any(map(sql.upper().__contains__, MOD_COMMANDS)):
        raise ValueError(
            "fetch_query() -> SQL contains modification commands, if you want "
            "to allow modifications in the SQLite database set parameter "
            "_allow_modifications_ to True."
        )
    with get_db_connection(db_filepath=db_filepath) as conn:
        return pd.read_sql(sql, con=conn)


def execute_query(sql: str, db_filepath: str = DB_FILEPATH):
    """Executes any SQLite query given as input in the database stored in
    db_filepath.

    Parameters
    ----------
    sql : str
        SQLite query to be executed
    db_filepath : str, optional
        file path where the splite database is stored, by default DB_FILEPATH
    """
    with get_db_connection(db_filepath=db_filepath) as conn:
        conn.cursor().execute(sql)
        conn.commit()
