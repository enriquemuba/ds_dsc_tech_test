
# Introduction

Hello applicant, we are excited to welcome you to our technical test! We believe that you 
are a great fit for the position and would like see how you handle a data science challenge 
with no straightforward solution. Please consider the following problem: 

A hotelier customer wants to create a marketing campaign in Tripadvisor.  
The hotelier expects the campaign to meet a visibility target. In this case, the proportion of 
times that one of their ads has been displayed in first/top position (what we call Top Position 
Share or TPS). 
Given that we have performance data for each hotel that contains information about TPS and cost. 
We would like to know an estimate of the cost for the following month for an arbitrary campaign with an 
arbitrary target (e.g.: TPS = 50%). 

It is important to note that a campaign is defined as a set of placements, where a placement is a combination of 
hotel + user country + user device. Your aim is to forecast the cost that a campaign will produce in the following month, given an TPS target. 
TPS can be calculated dividing top position impressions by eligible impressions (Top Position Share (TPS) = top_position_impressions / eligible_impressions).

You can expect a request that includes a set of hotels, a set of user countries and a set of devices, together with 
a target TPS. And you should return a cost estimate.

## Goals

The goal of this exercise is to get to know your working style, technical and coding skills and analytical thinking. 
Achieving a very high performance is not the main goal. Hence, we recommend that you do not spend too much time tuning your solution.

Feel free to use as many resources and tools as you consider necessary, and make sure to add comments and explanations where you 
think they might be valuable for us to understand your implementation.

## The data

For this exercise we have prepared a dataset with the performance data of a certain set of hotels in Tripadvisor metasearch channel.
In particular, we provide you with 4 months (Nov 2021, Dec 2021, Jan 2022, Feb 2022) of data including the 
clicks, cost, impressions...  
The data comes as a sqlite database with the following structure:
* dim_hotel: contains descriptions of hotels
* dim_placement: contains descriptions of placements
* topic_ta_visibility: contains visibility data at placement + date level
* topic_ta_clicks: contains data about each click during the aforementioned period.

You can read more details about the data in the [Apendix](##Apendix) section below.


## Technical requirements

The test must be solved using _Python_. We also recommend you to use _git_ to maintain your code.
In addition, make sure that we can reproduce your code. For that we suggest you pack your
environment using [Anaconda](https://www.anaconda.com/distribution/) or even [Docker](https://www.docker.com/) 
(note that Docker images can be shared publicly with [DockerHub](https://hub.docker.com/)).

## Submission of the results

Please, bundle all of your code and explanation as a zip file and send them back as described in the e-mail you received.

Do not hesitate to contact us in case you have any question. We are also happy to get short feedback on the test itself. 

Have fun!

## Appendix

The data for this problem can be found in the folder _data_.
It is a sqlite database file containing the following tables:
* dim_hotel:
  * hotel_id
  * hotel_country: the country of the hotel.
  * hotel_city: the city of the hotel.
  * state: the state of the hotel.
* dim_placement:
  * placement_id
  * hotel_id
  * user_country: the country of the user
  * user_device: the device of the user
* topic_ta_visibility: contains the visibility data. It is aggregated at placement_id + report_date level.
  * report_date: date of the report
  * placement_id
  * impressions: number of total ad impressions
**Note:** An impression is registered when your ad is displayed in a user search.
  * eligible_impressions: total number of impressions in which the ad was eligible (even if it was not displayed)
  * avg_position: Average ad position of the hotel website in the channel ranking
  * slot_position_impressions: number of ad impressions that were displayed in a position visible by the user
  * top_position_impressions: number of ad impressions that were displayed in the top position
* topic_ta_clicks: contains click and cost data. Each row represents a click for a placement_id + report_date + hour.
  * report_date: date of the report
  * report_hour: hour of the report
  * placement_id
  * cost: cost in dollars
  * click_id
  * display_rank: ad position of the hotel website in the channel ranking for the given click

**[Note]: Not all data is equally useful, so don't worry if you do not use all the attributes!**
**In addition, feel free to filter out some hotel data if the amount of data you have is too large for the computation power of your system.**

[Note]: To access the data from the sqlite database you can use the functions included in the script utils/db_utils.py. This script contains the following functions:
 * get_db_connection: Yields an sqlite3 connection to the database
 * fetch_query: Returns a pandas form of a SELECT type SQLite3 query.
 * execute_query: Executes any SQLite query given as input (can be useful if you need to modify the database).

For example, you can check the data in dim hotel table using the following code:
```
from utils.db_utils import fetch_query
print(fetch_query(sql="SELECT * FROM dim_hotel LIMIT 10000;"))
```

